@ECHO OFF
setlocal enableDelayedExpansion
REM  $----------------------------------------------------------------------------------------------------------------$
REM  PURPOSE    : Setup memory parameters for XHQCACHE oracle instance 
REM  USAGE      : set_memory_param.cmd sys_password 
REM  OBS        : Please make sure that file memory_parameters.sql is in the same directory as set_memory_param.cmd ! 
REM  $----------------------------------------------------------------------------------------------------------------$

:begin
if "%1"=="" goto parmMissingOrWrong
set log_file=memory_param.log

cls
echo ---------------------------------------------------------------------------------------------------
echo The script will check and set(if necessary) memory parameters for XHQCACHE oracle instance. 
echo For the new parameters to take effect, you^'ll need to restart the XHQ (shutdown complete)
set /p opt="Are you sure you want to proced now ? (Y/N) : " =
if /i %opt%==N (goto retry)
echo ----------------------------------------------------------------------------------------------------
set oracle_sid=XHQCACHE
sqlplus -s -L sys/"%1" as sysdba @memory_parameters.sql > %log_file% 2>&1

findstr /I /C:"ERROR" "%log_file%"
if errorlevel 0 if not errorlevel 1 goto logerrors
goto finished

:logerrors
cls
echo --------------------------------------
echo ERRORS found in "%log_file%"
echo Please review and/or email support.
goto end

:finished
cls
echo --------------------------------------------------------------------------------------------
echo The script has been executed successfully !
echo Please check the log file (%log_file%) in order to determine if a restart is needed or not !
goto end

:retry
echo ----------------------------------------------------
echo WARNING: The script was not runned at this time !
echo Please retry when you can restart the app/database.
goto end

:parmMissingOrWrong
echo --------------------------------------------------------------------------------
ECHO ERROR: Please provide all the required parameters in order to run the script !
ECHO USAGE: set_memory_param.cmd XhqCache_sys_password
echo --------------------------------------------------------------------------------
goto end

:end
endlocal
