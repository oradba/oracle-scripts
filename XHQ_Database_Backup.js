// XHQ Embedded Database Interactive Backup and Migration Utility
// 2012 Siemens Energy & Automation, Inc.
//
// Supported Versions: Released for Siemens XHQ 4.5 SP1 B221 and 4.5 SP2 B221 or later usage only
//
// Contact: Siemens Energy & Automation, Inc., XHQ Customer Support
//          6 Journey, Suite 200, Aliso Viejo, CA 92656, USA
//
// Date: 23 October 2012
//

var debug=false;
var sSourceDir;
var sOutputDir;
var sSourceExt="No.Files.Found";
var sConversionXSL="No.Files.Found";
var sDefaultComponent,sDefaultComponentPath;
var sMappingFile;
var sHost;
var arrComponents = new Array();
var arrErrorList = new Array();
var oFso;
var xmlDoc;
var xslDoc,xslTemplate;
var outDoc;
var xmlMapping;
var bRecursive;
var objExplorer;
var iTotalFiles=0;
var iCurrentFile=0;
var WshShell;
var WshSysEnv;
var sAccess;
var sConversionFormat;
var sApp="";
//
// Siemens XHQ Embedded Database Interactive Backup and Migration Utility Version
//
var cVersion="Version 3.7"
var ENV_XHQ_DEV_HOME;
var ReposCreateDir
var PhysicalName;
var DomainName;
var XHQ_Major;
var CurrentDirectory;
var oFso = new ActiveXObject("Scripting.FileSystemObject");

	if ((WScript.Arguments.length==0) || (settingParm("?","")!="") || (settingParm("h","")!="")) {
		printHelp();
		WScript.Quit();
	}
	
	//get Operating System Properties
	var os = new OS();
	if (debug) {
		WScript.echo ("OS Version: " + os.version + ' ' + os.name);
		WScript.echo ("\tisVista : " + ((os.isVista) ? "true" : "false"));
		WScript.echo ("\tis2008  : " + ((os.is2008) ? 'true' : 'false'));
		WScript.echo ("\tis2008R2: " + ((os.is2008R2) ? 'true' : 'false'));
		WScript.echo ("\tis7     : " + ((os.is7) ? 'true' : 'false'));
	}
	
	//
	// Get Parameters from Environment etc.
	//
	WshShell = new ActiveXObject("WScript.Shell");
	WshSysEnv = WshShell.Environment("Process");
	
	ENV_XHQ_DEV_HOME = WshSysEnv("XHQ_DEV_HOME");
	ENV_XHQ_SERV_HOME = WshSysEnv("XHQ_SERVER_HOME");
	if (os.is2008R2) {		
		ENV_XHQ_DB_HOME = WshSysEnv("XHQ_DBMS_HOME") +'\\'+WshSysEnv("XHQ_DBMS_VERSION");
	} else {		
		ENV_XHQ_DB_HOME = WshSysEnv("XHQ_DB_HOME");
	}
	
	if (os.is2008R2) {		
		// ENV_XHQ_DB_HOME = WshSysEnv("XHQ_DBMS_HOME") +'\\'+WshSysEnv("XHQ_DBMS_VERSION");
		ReposCreateDir = WshSysEnv("XHQ_DBMS_HOME") + "\\dbadmin\\create";
	} else {		
		ReposCreateDir = WshSysEnv("XHQ_SERVER_REPOS") + "\\..\\dbadmin\\XHQ\\create";
	}
	if (debug) {
		WScript.echo ("DB_HOME "+ReposCreateDir);    
		}
		
   	 // read server domain name from the registry
   	try {
    		DomainName = '.' + WshShell.RegRead("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\NV Domain");
        } catch(e) {
        	DomainName = '';
        }
        if (debug)
	        WScript.echo ("Qualifying Domain is: " + DomainName + "");
        
        
	if (ENV_XHQ_SERV_HOME == "") {
		WScript.echo ("Environment variables missing. XHQ Solution Migration cannot work.\n\tExecution aborted!");
		WScript.Quit();
	}
	
	// read the XHQ Server major value
	var execResult = WshShell.Exec('"%XHQ_SERVER_HOME%/jre/bin/java" -cp "%XHQ_SERVER_HOME%/lib/connector.jar" net.indx.xhq.BuildInfo');
	var output = execResult.StdOut.readAll()
	XHQ_Major = output.match("XHQ Version\\s([0-9]+)\..*")[1];
	if (debug) 
		WScript.echo ("XHQ Major Version is "+XHQ_Major);

    // get the physical computer name from the registry
	PhysicalName = WshShell.RegRead("HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Services\\Tcpip\\Parameters\\NV Hostname");
    if (PhysicalName == "") PhysicalName="THISMACHINE";
	
	CurrentDirectory=WshShell.CurrentDirectory;
    // Export from source 
    if (settingParm("exp","")!="") {
	  XHQ_ExpDB();
	}

	// Import into destination 
	if (settingParm("imp","")!="") {
		XHQ_ImpDB();
	}

	// Rescue
	if (settingParm("rescue","")!="") {
		XHQ_RescueDB();
    }


function OS() { 
	var wmi = GetObject("winmgmts:{impersonationLevel=Impersonate}\\\\.\\root\\cimv2");
	var query = "Select * From Win32_OperatingSystem";
	e = new Enumerator(wmi.ExecQuery(query));
	var data = e.item();

	this.windowsSKU = data.OpertingSystemSKU;
	this.serial = data.SerialNumber;
	this.build = data.BuildNumber;
	this.buildType = data.BuildType;
	this.servicePackMajor = data.ServicePackMajorVersion;
	this.servicePackMinor = data.ServicePackMinorVersion;
	this.version = data.Version;
	this.caption = data.Caption;
	this.name = data.Name;
	this.name = this.name.substring(0, this.name.indexOf("|")-1);
	this.isVista = this.version.substring(0,4) == "6.0." && this.name.indexOf('2008') < 0;
	this.is2008 = this.version.substring(0,4) == "6.0." && this.name.indexOf('2008') > 0;
	this.is2008R2 = this.version.substring(0,4) == "6.1." && this.name.indexOf('2008') > 0;
	this.is7 = this.version.substring(0,4) == "6.1." && this.name.indexOf('2008') < 0;
}

// Fetch formatted sysdate from Oracle
function fetchSysdate(XHQpwd, XHQTNS) {
	//prepare sysdate.sql
	var fout = oFso.OpenTextFile("sysdate.sql", 2, true);
	fout.writeline("select '##' || to_char(sysdate, 'mm.dd.yyyy hh24:mi:ss')||'##' as sys_date from dual;");
	fout.writeline("exit;");
	fout.Close();
		
	WshSysEnv("oracle_sid") = "XHQ";
	var execResult = WshShell.Exec('testdate.bat ' + XHQTNS + ' ' + XHQpwd );
	var output = execResult.StdOut.readAll();
	var sysdate = output.match(".*##(.+)##.*")[1];
	
	if (oFso.FileExists(CurrentDirectory+ "\\sysdate.sql"))
		oFso.DeleteFile(CurrentDirectory+ "\\sysdate.sql",true);
		
	return sysdate;
	
}

// Fetch destination machine id from Oracle
function fetchMachineId() {
	var XHQpwd = FetchXHQpwd();
	var XHQHISTpwd = FetchXHQHISTpwd();
	var XHQTNS = FetchTNSHost();

	//prepare machineid.sql
	var fout = oFso.OpenTextFile("machineid.sql", 2, true);
	fout.writeline("select '##' || machine_id ||'##' as machine_id from XHQ.XHQ_CAT_MACH;");
	fout.writeline("exit;");
	fout.Close();

	var execResult = WshShell.Exec('machineid.bat ' + XHQTNS + ' ' + XHQpwd );
	var output = execResult.StdOut.readAll();
	var matched = output.match(".*##(.+)##.*");
	if (matched == null) {
		return null;
	}

	if (oFso.FileExists(CurrentDirectory+ "\\machineid.sql"))
		oFso.DeleteFile(CurrentDirectory+ "\\machineid.sql",true);

	return matched[1];
}


//
//	Check the parameters for a specific option.
//	If the option is not specified, use the default value.
//
function settingParm(option,defaultValue) {
	var setting = defaultValue;
	var arg;
	var found = false;
	for (var i=0; (i<WScript.Arguments.length) && (!found); i++) {
		arg = String(WScript.Arguments(i)).toLowerCase();
		if ( (arg == "-"+option) || (arg == "/"+option)) {
			found = true;
			if (i==WScript.Arguments.length-1) setting ="none";
			else setting = WScript.Arguments(i+1);
		}
	}
	return (setting);
}

function printHelp() {
	WScript.echo (' SIMATIC IT XHQ Embedded Database Interactive Backup and Migration Utility '+cVersion+ '\n' +
					 '  2012 Siemens Energy & Automation, Inc. \n\n' +
					 ' Usage: XHQ_Database_Backup \n'  +
					 ' Options:\n' +
					 '    -?	Show This Message\n' +
                                         '    -exp	Export Embedded Database Data\n' +
                                         '    -imp	Import Embedded Database Data\n' +
                                         '    -rescue	Rescue Embedded Database Data (hard-coded IMPORT - use with caution!)\n');
}

function writeLogFile() {
	var i;
	var outFile = oFso.createTextFile(sOutputDir+"\\XHQVC.log")
	for (i=0;i<arrErrorList.length;i++)
		outFile.writeLine(arrErrorList[i]+"\n" );
	outFile.close();
};

function MkDataPumpDir(XHQpwd) {
	var ForReading = 1, ForWriting = 2;
	var fout = oFso.OpenTextFile("datapump.sql", ForWriting, true);
	fout.writeline("set echo off feedback off verify off");
	fout.writeline('CREATE OR REPLACE DIRECTORY MIGRATION AS \''+CurrentDirectory+'\';');
	fout.writeline("EXIT;");
	fout.Close();
	return "";
}

function MkXHQExpFile(XHQpwd) {
	var ForReading = 1, ForWriting = 2;
	var fout = oFso.OpenTextFile("XHQBKPEXP.PAR", ForWriting, true);
	fout.writeline('USERID=SYSTEM/"'+XHQpwd+'"');
	fout.writeline("DIRECTORY=migration");
	fout.writeline("SCHEMAS=XHQ,XHQAPPS,XHQSTATS");
        fout.writeline("EXCLUDE=TABLE:\"LIKE 'XHQ_T%%A'\",TABLE:\"LIKE 'XHQ_T%%B'\",TABLE:\"LIKE 'XHQ_T%%X'\",TABLE:\"LIKE 'XHQ_CACHE%'\"");
	fout.writeline("DUMPFILE=XHQORABACKUP.dmp");
	fout.writeline("LOGFILE=XHQBKPEXP.log");
	fout.writeline("REUSE_DUMPFILES=Y");
	fout.Close();
	return "";
}

function MkXHQExpFileHist(XHQHISTpwd) {
	var ForReading = 1, ForWriting = 2;
	var fout = oFso.OpenTextFile("XHQHBKPEXP.PAR", ForWriting, true);
	fout.writeline('USERID=SYSTEM/"'+XHQHISTpwd+'"');
	fout.writeline("DIRECTORY=migration");
	fout.writeline("SCHEMAS=XHQHIST");
	fout.writeline("DUMPFILE=XHQHORABACKUP.dmp");
	fout.writeline("LOGFILE=XHQHBKPEXP.log");
	fout.writeline("REUSE_DUMPFILES=Y");
	fout.Close();
	return "";
}

function MkXHQRescFile(XHQpwd,sHostName) {
	var ForReading = 1, ForWriting = 2;
	var FileName=sHostName;
	var fout = oFso.OpenTextFile("xhqrescuebkp.par", ForWriting, true);
	fout.writeline('USERID=SYSTEM/"'+XHQpwd+'"');
	fout.writeline("DIRECTORY=migration");
	fout.writeline("FULL=Y");
	fout.writeline("EXCLUDE=TABLE:\"LIKE 'XHQ_T%%A'\",TABLE:\"LIKE 'XHQ_T%%B'\",TABLE:\"LIKE 'XHQ_CACHE%'\"");
	fout.writeline("LOGFILE=XHQRescueBKP.log");
	fout.writeline("DUMPFILE="+FileName+".dmp");
	fout.writeline("REUSE_DUMPFILES=Y");
	fout.Close();
	return "";
}

function MkXHQRescFileHist(XHQHISTpwd,sHostName) {
	var ForReading = 1, ForWriting = 2;
	var FileName=sHostName;
	var fout = oFso.OpenTextFile("XHQHRESCUEBKP.PAR", ForWriting, true);
	fout.writeline('USERID=SYSTEM/"'+XHQHISTpwd+'"');
	fout.writeline("DIRECTORY=migration");
	fout.writeline("SCHEMAS=XHQHIST");
	fout.writeline("LOGFILE=XHQhRescueBKP.log");
	fout.writeline("DUMPFILE="+FileName+".dmp");
	fout.writeline("REUSE_DUMPFILES=Y");
	fout.Close();
	return "";
}

function MkXHQImpFile(XHQpwd) {
	var ForReading = 1, ForWriting = 2;
	var fout = oFso.OpenTextFile("XHQBKPIMP.PAR", ForWriting, true);
	fout.writeline('USERID=SYSTEM/"'+XHQpwd+'"');
	fout.writeline("DIRECTORY=MIGRATION");
	fout.writeline("TABLE_EXISTS_ACTION=REPLACE");
	fout.writeline("REMAP_TABLESPACE=(XHQCACHE_DATA1:XHQ_DATA_SMALL)");
        fout.writeline("FULL=Y");
	fout.writeline("DUMPFILE=XHQORABACKUP.dmp");
	fout.writeline("LOGFILE=XHQBKPIMP.log");
	fout.Close();
	return "";
}

function MkXHQRescueImpFile(XHQpwd,sHostName) {
	var ForReading = 1, ForWriting = 2;
	var FileName=sHostName;
	var fout = oFso.OpenTextFile("XHQBKPIMPRescue.PAR", ForWriting, true);
	fout.writeline('USERID=SYSTEM/"'+XHQpwd+'"');
	fout.writeline("DIRECTORY=MIGRATION");
	fout.writeline("TABLE_EXISTS_ACTION=REPLACE");
	fout.writeline("FULL=Y");
	fout.writeline('DUMPFILE="'+FileName+'.dmp"');
	fout.writeline("LOGFILE=XHQRescueIMP.log");
	fout.Close();
	return "";
}

function MkXHQHRescueImpFile(XHQHISTpwd,sHostName) {
	var ForReading = 1, ForWriting = 2;
	var FileName=sHostName;
	var fout = oFso.OpenTextFile("XHQHBKPIMPRescue.PAR", ForWriting, true);
	fout.writeline('USERID=SYSTEM/"'+XHQHISTpwd+'"');
	fout.writeline("DIRECTORY=MIGRATION");
	fout.writeline("TABLE_EXISTS_ACTION=REPLACE");
	fout.writeline("FULL=Y");
	fout.writeline("DUMPFILE="+FileName+".dmp");
	fout.writeline("LOGFILE=XHQHRescueIMP.log");
	fout.Close();
	return "";
}

function MkXHQImpFileHist(XHQHISTpwd) {
	var ForReading = 1, ForWriting = 2;
	var fout = oFso.OpenTextFile("xhqhistbkpimp.PAR", ForWriting, true);
	fout.writeline('USERID=SYSTEM/"'+XHQHISTpwd+'"');
	fout.writeline("DIRECTORY=MIGRATION");
	fout.writeline("FULL=Y");
	fout.writeline("DUMPFILE=XHQHORABACKUP.dmp");
	fout.writeline("LOGFILE=XHQHORABACKUP.log");
	fout.Close();
	return "";
}

function MkXHQddl(XHQpwd) {
	var ForReading = 1, ForWriting = 2;
	var fout = oFso.OpenTextFile("ddl.sql", ForWriting, true);
	fout.writeline("set echo off feedback off verify off time on");
	fout.writeline("PROMPT Terminating connections");
	fout.writeline("begin");
	fout.writeline("for i in (select sid,serial# from v$session where username not in ('SYS','SYSTEM') and type != 'BACKGROUND') LOOP");
	fout.writeline("execute immediate 'alter system kill session '''||i.sid||','||i.serial#||''' immediate';");
	fout.writeline("end loop;");
	fout.writeline("end;");
	fout.writeline("/");
	fout.writeline("PROMPT Dropping constraints");
	fout.writeline("begin");
	fout.writeline("for i in (select constraint_name, table_name, owner from dba_constraints where owner in ('XHQ','XHQAPPS','XHQSTATS') and constraint_type in ('P','R','U')) LOOP");
	fout.writeline("execute immediate 'alter table '||i.owner||'.'||i.table_name||' disable constraint '||i.constraint_name||'';");
	fout.writeline("end loop;");
	fout.writeline("end;");
	fout.writeline("/");
	fout.writeline("PROMPT Dropping tables");
	fout.writeline("begin");
	fout.writeline("for i in (select table_name, owner from dba_tables where owner in ('XHQ','XHQAPPS','XHQSTATS')) LOOP");
	fout.writeline("execute immediate 'drop table '||i.owner||'.'||i.table_name||' cascade constraints';");
	fout.writeline("end loop;");
	fout.writeline("end;");
	fout.writeline("/");
	fout.writeline("PROMPT Dropping objects");
	fout.writeline("begin");
	fout.writeline("for i in (select object_name, object_type, owner from dba_objects where owner in ('XHQ','XHQAPPS','XHQSTATS') and object_type in ('PROCEDURE','FUNCTION','VIEW','SEQUENCE')) LOOP");
	fout.writeline("execute immediate 'drop '||i.object_type||' '||i.owner||'.'||i.object_name||'';");
	fout.writeline("end loop;");
	fout.writeline("end;");
	fout.writeline("/");
	fout.writeline("create role XHQ_CACHE_SEL;");
	fout.writeline("create role XHQ_RDS_UPDT;");
	fout.writeline("EXIT;");
	fout.Close();
	return "";
}

function MkXHQddlHist(XHQHISTpwd) {
	var ForReading = 1, ForWriting = 2;
	var fout = oFso.OpenTextFile("ddl_hist.sql", ForWriting, true);
	fout.writeline("set echo off feedback off verify off");
	fout.writeline("PROMPT Terminating XHQHIST connections");
	fout.writeline("begin");
	fout.writeline("for i in (select sid,serial# from v$session where username not in ('SYS','SYSTEM') and type != 'BACKGROUND') LOOP");
	fout.writeline("execute immediate 'alter system kill session '''||i.sid||','||i.serial#||''' immediate';");
	fout.writeline("end loop;");
	fout.writeline("end;");
	fout.writeline("/");
	fout.writeline("PROMPT Dropping XHQHIST constraints");
	fout.writeline("begin");
	fout.writeline("for i in (select constraint_name, table_name, owner from dba_constraints where owner='XHQHIST' and constraint_type in ('P','R','U')) LOOP");
	fout.writeline("execute immediate 'alter table '||i.owner||'.'||i.table_name||' disable constraint '||i.constraint_name||'';");
	fout.writeline("end loop;");
	fout.writeline("end;");
	fout.writeline("/");
	fout.writeline("PROMPT Dropping XHQHIST tables");
	fout.writeline("begin");
	fout.writeline("for i in (select table_name, owner from dba_tables where owner='XHQHIST') LOOP");
	fout.writeline("execute immediate 'drop table '||i.owner||'.'||i.table_name||' cascade constraints';");
	fout.writeline("end loop;");
	fout.writeline("end;");
	fout.writeline("/");
	fout.writeline("PROMPT Dropping XHQHIST objects");
	fout.writeline("begin");
	fout.writeline("for i in (select object_name, object_type, owner from dba_objects where owner='XHQHIST' and object_type in ('PROCEDURE','FUNCTION','VIEW','SEQUENCE')) LOOP");
	fout.writeline("execute immediate 'drop '||i.object_type||' '||i.owner||'.'||i.object_name||'';");
	fout.writeline("end loop;");
	fout.writeline("end;");
	fout.writeline("/");
	fout.writeline("EXIT;");
	fout.Close();
	return "";
}

function MkXHQdml(XHQpwd) {
	var ForReading = 1, ForWriting = 2;
	var fout = oFso.OpenTextFile("update.sql", ForWriting, true);
	fout.writeline("set echo off feedback off verify off time on");
	fout.writeline("spool ddlxhq.out");
	fout.writeline("UPDATE XHQ_CAT_MACH set updt_timestamp=sysdate, machine_id=lower('"+PhysicalName+DomainName+"');");
	fout.writeline("UPDATE XHQ_SOL_MACH set updt_timestamp=sysdate, machine_id=lower('"+PhysicalName+DomainName+"');");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_OBJECT_update;");
	fout.writeline("DELETE XHQ_CAT_CMPNT;");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_DELETES;");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_DYNAMIC_OBJECT_QUEUE;");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_OBJECT_ASSOCIATION;");
	fout.writeline("DELETE XHQ_SOL_OBJECT_MEMBER;");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_VIEW_STAT_RPT;");
	//CR21453
	//fout.writeline("DELETE XHQ_CAT_ROLE;");
	//fout.writeline("TRUNCATE TABLE XHQ_CAT_ROLE_PERMISSIONS;");
	//fout.writeline("TRUNCATE TABLE XHQ_CAT_USER_ROLE;");
	fout.writeline("TRUNCATE TABLE XHQ_CAT_VIEW;");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_ALIAS_REFS;");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_ALIAS_REFS_STG;");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_CURR_CMPNT;");
	fout.writeline("TRUNCATE TABLE XHQ_SOL_STATIC_NAMESPACE;");
	fout.writeline("DELETE XHQ_SOL_OBJECT;");
	fout.writeline("INSERT INTO XHQ_ADM_KEY_VALUE (keyname, value, updt_timestamp) values('SOL_EXPORT_DIRTY', 'TRUE', sysdate);");
	fout.writeline("UPDATE XHQ_CATALOG set catalog_rev=0;");
	fout.writeline("UPDATE XHQ_ADM_KEY_VALUE SET VALUE='2' WHERE KEYNAME='SCHEMA_MAJOR';");
	fout.writeline("UPDATE XHQ_ADM_KEY_VALUE SET VALUE='0' WHERE KEYNAME='SCHEMA_MINOR';");
	fout.writeline("COMMIT;");
	fout.writeline("spool off;");
	if (!os.is2008R2) {		
		fout.writeline("@"+ReposCreateDir+"\\ddlupdate.sql");
		fout.writeline("@"+ReposCreateDir+"\\dbviews.sql");
		fout.writeline("@"+ReposCreateDir+"\\dmlupdate.sql");
		fout.writeline("@"+ReposCreateDir+"\\cr11658.sql");
		fout.writeline("@"+ReposCreateDir+"\\dbversion.sql");
		fout.writeline("@"+ReposCreateDir+"\\ddlo.sql");
		fout.writeline("@"+ReposCreateDir+"\\ddloupdates.sql");
	}
	fout.writeline("EXIT;");
	fout.Close();
	return "";
}


//
// Run-time prefix
//
function XHQRunTime() {
	var d = new Date();
	var currDay= new String();
	//var currTime= new String();
	currDay=currDay.concat(d.getFullYear().toString(),LZ((d.getMonth())+1),LZ(d.getDate()),"_");
	//currDay=currDay.concat(LZ(d.getHours()),LZ(d.getMinutes()),LZ(d.getSeconds()),"_");
	return currDay;
}


//
// Function to left-pad a string with zero
//
function LZ(theInt){
	if (theInt < 10) {
		return '0'+theInt.toString()
	} else {
		return theInt.toString();
	};
}

//
// Fetches password
// for 2008R2 uses pwutil
// else uses storage.properties files in the destination solution's repos directory
//
function FetchXHQpwd() {
	var ForReading = 1, ForWriting = 2;
	var Line;
	var XHQpwd="";
	if (os.is2008R2) {
		objExecObject = WshShell.Exec('"%XHQ_DBMS_HOME%\\dbadmin\\bin\\pwutil.bat" -g xhq.xhq');
		var strText = objExecObject.StdOut.ReadLine();		//response is like xhq.xhq=pass
		XHQpwd = strText.substring(8);	//capture response following = sign
		if (debug)
			WScript.echo ("Database password retrieved (R2)... <hidden>");
	} else {
		var ServerReposDir = WshSysEnv("XHQ_SERVER_REPOS");
		if (oFso.FileExists(ServerReposDir+"\\storage.properties")) {
			var fin = oFso.OpenTextFile(ServerReposDir+"\\storage.properties", ForReading);
			while (!fin.AtEndOfStream)	{
				Line = fin.ReadLine();
				//
				// Process file's lines in search for the password setting
				//
				XHQpwdKeyword=Line.substring(0,16);
				if (XHQpwdKeyword=="db.jdbc.password")	{
					XHQpwd=Line.substring(17,Line.length);
					if (debug)
						WScript.echo ("Database password retrieved ... <hidden>");
					break;
				}
			}
			fin.Close();
		} else if (debug) {
			WScript.echo ("Missing database password retrieval file:\n\t"
						+ ServerReposDir+"\\storage.properties");
			WScript.echo ("Execution aborted!");
			oFso.DeleteFile(CurrentDirectory+ "\\*.sql",true);
			oFso.DeleteFile(CurrentDirectory+ "\\*.par",true);
			if (oFso.FileExists(CurrentDirectory+ "\\extendcolumns.log")) 	oFso.DeleteFile(CurrentDirectory+ "\\extendcolumns.log",true);		
			if (oFso.FileExists(CurrentDirectory+ "\\ddlxhq.out")) 	oFso.DeleteFile(CurrentDirectory+ "\\ddlxhq.out",true);				
			WScript.Quit(1);
		}
	}
	return XHQpwd;
}

function FetchXHQHISTpwd() {
	var XHQHISTpwd="";
	if (os.is2008R2) {
		objExecObject = WshShell.Exec('"%XHQ_DBMS_HOME%\\dbadmin\\bin\\pwutil.bat" -g xhqhist.xhqhist');
		var strText = objExecObject.StdOut.ReadLine();		//response is like xhq.xhq=pass
		XHQHISTpwd = strText.substring(16);	//capture response following = sign
		if (debug)
			WScript.echo ("R2 XHQhist password retrieved ... " +XHQHISTpwd );
	} else {
		 XHQHISTpwd = FetchXHQpwd();
	}
	return XHQHISTpwd;
}

//
// Fetches host value from the listener.ora file
//	
function FetchTNSHost()	{
	var ForReading = 1, ForWriting = 2;
	var Line;
	var XHQTNS="";
	var tmpOpen = ENV_XHQ_DB_HOME+"\\network\\admin\\listener.ora";
		
	if (oFso.FileExists(tmpOpen)) {
		var fin = oFso.OpenTextFile(tmpOpen, ForReading);
		if (debug)
			WScript.echo ("listener.ora file found");    

		while (!fin.AtEndOfStream) {
			Line = fin.ReadLine();
			//
			// Process listener.ora and search for the HOST setting
			//      
			var hostindex = Line.indexOf('HOST');
			if (hostindex != -1){
				var equalindex= Line.indexOf('=',hostindex);
				var paranindex= Line.indexOf(')',hostindex);
				XHQTNS=Line.substring(equalindex+1,paranindex);//.trim();
				break;
			}
		}
		if (debug){
			if (XHQTNS=="")
				WScript.echo ("WARNING - Listener host name NOT retrieved");
			else 
				WScript.echo ("Listener host name retrieved ..."+XHQTNS);
		}

		fin.Close();
	} else {
		WScript.echo ("listener.ora file missing (continuing)");
	}
	return XHQTNS;
}

function XHQ_ExpDB() {
	var XHQpwd;
	var XHQHISTpwd;
	var stdin = WScript.StdIn;
	
	if (!debug) WScript.echo (' Migration Utility '+cVersion+'\n' +
				'  2012, Siemens Energy & Automation, Inc. \n\n' +
			' Export of the Database Data ... \n');
		   
	XHQpwd = FetchXHQpwd();
	XHQHISTpwd = FetchXHQHISTpwd();
	XHQTNS = FetchTNSHost();
	
	if (debug) WScript.echo ("ExpDB R2 password retrieved: " + XHQpwd);
	

	//var sysdate = fetchSysdate(XHQpwd,XHQTNS);

	MkDataPumpDir(XHQpwd);
		
	if (debug)
		WScript.echo ('XHQ Major Version is: '+XHQ_Major+'');
		
	try {
		MkXHQExpFile(XHQpwd);
		if (debug) {
			WScript.echo ('FQDN is: '+PhysicalName+DomainName);
		//	WScript.echo ("Database ready at: "+sysdate);
			WScript.echo ("Exporting XHQ");
		}
		
		if (oFso.FileExists(CurrentDirectory+ "\\XHQORABACKUP.dmp")) {
			oFso.DeleteFile(CurrentDirectory+ "\\XHQORABACKUP.dmp",true);
		}
		
		if (debug) WScript.echo ('before XHQ datapump');
		
		WshSysEnv("oracle_sid") = "XHQ";
		var runSQL = WshShell.Run('sqlplus -S system/'+XHQpwd+' @datapump.sql',1,true);
		//var runSQL = WshShell.Run('sqlplus -S /nolog @datapump.sql',1,true);			       		
		WshSysEnv("oracle_sid")="XHQ";
		var ret = WshShell.Run('expdp parfile=xhqbkpexp.par',1,true);

		// export Data Recorder only if Major XHQ Version is 4.x
		if (XHQ_Major == 4) {
			MkXHQExpFileHist(XHQHISTpwd);
			if (debug)
				WScript.echo ("Exporting XHQHIST");
			if (oFso.FileExists(CurrentDirectory+ "\\XHQHORABACKUP.dmp")) {  
				oFso.DeleteFile(CurrentDirectory+ "\\XHQHORABACKUP.dmp",true);
			}		  			
		
			WshSysEnv("oracle_sid")="XHQHIST";	
			var runSQL = WshShell.Run('sqlplus -S system/'+XHQHISTpwd+' @datapump.sql',1,true);
			WshSysEnv("oracle_sid")="XHQHIST";	
			var ret = WshShell.Run('expdp parfile=xhqhbkpexp.par',1,true);
			
		}

		if (debug) {
			WScript.echo ("Database Backup exported to XHQORABACKUP.dmp in "+CurrentDirectory);
			WScript.echo ('Deleting "'+CurrentDirectory+'\\datapump.sql"');
		}
		
		if (oFso.FileExists(CurrentDirectory+ "\\datapump.sql")) {
			oFso.DeleteFile(CurrentDirectory+ "\\datapump.sql",true);  
			oFso.DeleteFile(CurrentDirectory+ "\\*.par",true);
			if (oFso.FileExists(CurrentDirectory+ "\\extendcolumns.log")) 	oFso.DeleteFile(CurrentDirectory+ "\\extendcolumns.log",true);		
		}
		WScript.echo ("End of the Database Migration Utility");
	}
	catch (e){
		WScript.echo ("Database connectivity problem: "+e.description);
	}
}

                
function XHQ_ImpDB() {
	var DestMachineID="";
	var XHQpwd;
	var XHQHISTpwd;
	var XHQTNS;
	var stdin = WScript.StdIn;
	
	
	if (!debug) WScript.echo (' Migration Utility '+cVersion+'\n' +
		 '  2012, Siemens Energy & Automation, Inc. \n\n' +
		 ' Importing Database Data ... \n');
   	
   	XHQpwd = FetchXHQpwd();
	XHQHISTpwd = FetchXHQHISTpwd();
	XHQTNS = FetchTNSHost();
        
   	var runTimePrefix = XHQRunTime();
	var RescueFileName=runTimePrefix+PhysicalName;
	var RescueFileNameHist=runTimePrefix+"HIST"+PhysicalName;
	
	MkXHQImpFile(XHQpwd);
	MkXHQImpFileHist(XHQHISTpwd);
	MkXHQRescFile(XHQpwd,RescueFileName);
	MkXHQRescFileHist(XHQHISTpwd,RescueFileNameHist);
	MkXHQddl(XHQpwd);
	MkXHQddlHist(XHQHISTpwd);
	MkXHQdml(XHQpwd);
	MkDataPumpDir(XHQpwd);

	if (debug )
		WScript.echo ("Checking database availability.");              

	var sysdate = fetchSysdate(XHQpwd,XHQTNS);
	if (debug) {
		WScript.echo ("This server name is: "+PhysicalName);
		WScript.echo ("Database ready at: "+sysdate);
	}
             
	if (oFso.FileExists(CurrentDirectory+ "\\"+RescueFileName+".dmp")) {  
		oFso.DeleteFile(CurrentDirectory+ "\\"+RescueFileName+".dmp",true);
	}		
	
	WScript.echo ("Making a backup of current data.");
	
	if (debug ) WScript.echo ("Connecting to the XHQ instance.");
	WshSysEnv("oracle_sid")="XHQ";     		
	var runDataPumpXHQ = WshShell.Run("sqlplus -S system/"+ XHQpwd +" @datapump.sql",1,true);
	
	WshSysEnv("oracle_sid")="XHQ";
	var retBkp = WshShell.Run('expdp parfile=xhqrescuebkp.par',1,true);
	if (debug)
		WScript.echo ("Rescue Backup "+RescueFileName+".dmp has been created. ");
	
	// if Major XHQ Version is 4.x, grab the Recorder as well
	if (XHQ_Major == 4)	{  
		if (debug ) WScript.echo ("Connecting to the XHQHIST instance.");
		WshSysEnv("oracle_sid")="XHQHIST";
		var runDataPumpXHQ = WshShell.Run('sqlplus -S system/'+XHQHISTpwd+' @datapump.sql',1,true);
		WshSysEnv("oracle_sid")="XHQHIST";
		var retBkp = WshShell.Run('expdp parfile=xhqhrescuebkp.par',1,true);
		if (debug)
			WScript.echo ("Rescue Backup "+RescueFileNameHist+".dmp has been created. ");
	}
	
	var BtnCode = WshShell.Popup("Do you want to proceed with the Database import?",500, "                      *** WARNING ***", 4 + 16);
	if (BtnCode == 7) {
		WScript.echo ("Aborted!  Database has NOT been changed.");
		oFso.DeleteFile(CurrentDirectory+ "\\*.sql",true);
		oFso.DeleteFile(CurrentDirectory+ "\\*.par",true);
		if (oFso.FileExists(CurrentDirectory+ "\\extendcolumns.log")) 	oFso.DeleteFile(CurrentDirectory+ "\\extendcolumns.log",true);	
		if (oFso.FileExists(CurrentDirectory+ "\\ddlxhq.out")) 	oFso.DeleteFile(CurrentDirectory+ "\\ddlxhq.out",true);			
		WScript.Quit(1);
	}

	BtnCode = WshShell.Popup("The Database will be updated and all previous data will be lost. Do you wish to proceed ?",500, "                                                 ***** WARNING *****", 4 + 16);
	if (BtnCode == 7) {
		WScript.echo ("Aborted!  Database has NOT been changed.");
		oFso.DeleteFile(CurrentDirectory+ "\\*.sql",true);
		oFso.DeleteFile(CurrentDirectory+ "\\*.par",true);
		if (oFso.FileExists(CurrentDirectory+ "\\extendcolumns.log")) 	oFso.DeleteFile(CurrentDirectory+ "\\extendcolumns.log",true);	
		if (oFso.FileExists(CurrentDirectory+ "\\ddlxhq.out")) 	oFso.DeleteFile(CurrentDirectory+ "\\ddlxhq.out",true);	
		WScript.Quit(1);
	}

	if (debug)
		BtnCode = WshShell.Popup("The XHQ Server will now be stopped.",500, "              ***** WARNING *****", 0 + 64);
	
	var ShutDown = '"'+ENV_XHQ_SERV_HOME+"\\bin\\xhqboot.bat"+'"'+" shutdown";
	WScript.echo ("Shutting down XHQ Server.");
	retBkp = WshShell.Run(ShutDown,1,true);
		
	WScript.echo ("Importing data... please wait ");

	WshSysEnv("oracle_sid")="XHQ";
	if (debug) WScript.echo ("Recreating users. ");
	var runSQL = WshShell.Run('sqlplus -S system/"'+XHQpwd+'" @ddl.sql',1,true);
	
	WshSysEnv("oracle_sid")="XHQ";
	if (debug) WScript.echo ("Importing XHQ data step1. ");
	var ret = WshShell.Run('impdp parfile=xhqbkpimp.par',1,true);
	
	WshSysEnv("oracle_sid")="XHQ";
	if (debug) WScript.echo ("Running update.sql. ");
	var runUpdate = WshShell.Run('sqlplus -S xhq/"'+XHQpwd+'" @update.sql',1,true);
	if (os.is2008R2) {
	if (debug) WScript.echo ("Running UpgradeXHQ...");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\UpgradeXHQ.sql "'+XHQpwd+'"',1,true);	
	if (debug) WScript.echo ("Running ddlDI. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\ddlDI.sql "'+XHQpwd+'"',1,true);	
	if (debug) WScript.echo ("Running ddlLO. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\ddlLO.sql "'+XHQpwd+'"',1,true);
	if (debug) WScript.echo ("Running ddlLO-201206. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\ddlLO-201206.sql "'+XHQpwd+'"',1,true);
	if (debug) WScript.echo ("Running ddlLO-201207. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\ddlLO-201207.sql "'+XHQpwd+'"',1,true);
	if (debug) WScript.echo ("Running ddlLO-201208. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\ddlLO-201208.sql "'+XHQpwd+'"',1,true);
	if (debug) WScript.echo ("Running funcs. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\funcs.sql "'+XHQpwd+'"',1,true);
	if (debug) WScript.echo ("Running views. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\views.sql "'+XHQpwd+'"',1,true);
	if (debug) WScript.echo ("Running procs. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\procs.sql "'+XHQpwd+'"',1,true);
	if (debug) WScript.echo ("Running trigs. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\trigs.sql "'+XHQpwd+'"',1,true);
	if (debug) WScript.echo ("Running sys_xhq. ");
		var runUpdate = WshShell.Run('sqlplus -S /nolog @%XHQ_DBMS_HOME%\\dbadmin\\create\\SYS_XHQ.sql "'+XHQpwd+'"',1,true);
		}

	
	// import Data Recorder only if Major XHQ Version is 4.x
	if (XHQ_Major == 4)	{ 
		WshSysEnv("oracle_sid") = "XHQHIST";
		if (debug) WScript.echo ("Importing XHQHIST data step 1. ");
		var runSQLhist = WshShell.Run('sqlplus -S system/"'+XHQHISTpwd+'" @ddl_hist.sql',1,true);
		
		WshSysEnv("oracle_sid")="XHQHIST";
		if (debug) WScript.echo ("Importing XHQHIST data step 2. ");
		var ret = WshShell.Run('impdp parfile=xhqhistbkpimp.par',1,true);
		if (!os.is2008R2) runSQLhist = WshShell.Run('sqlplus -S system/"'+XHQHISTpwd+'" " + ReposCreateDir + "\\ddlupdatehist.sql "'+XHQHISTpwd+'"',1,true);
	}
		
	if (debug)
		WScript.echo ("Database imported from XHQORABACKUP.dmp in "+CurrentDirectory);
	else {
		oFso.DeleteFile(CurrentDirectory+ "\\*.sql",true);
		oFso.DeleteFile(CurrentDirectory+ "\\*.par",true);
		if (oFso.FileExists(CurrentDirectory+ "\\extendcolumns.log")) 	oFso.DeleteFile(CurrentDirectory+ "\\extendcolumns.log",true);		
		if (oFso.FileExists(CurrentDirectory+ "\\ddlxhq.out")) 	oFso.DeleteFile(CurrentDirectory+ "\\ddlxhq.out",true);	
	}	
	WScript.echo ("End of the Migration Import Utility.");
}

 function XHQ_RescueDB() {
	var DestMachineID="";
	var XHQpwd;
	var XHQHISTpwd;
	var XHQTNS;
	var stdin = WScript.StdIn;
	var runTimePrefix = XHQRunTime();
	var RescueFileName=runTimePrefix+PhysicalName;
	var RescueFileNameHist=runTimePrefix+"HIST"+PhysicalName;
	
	var RescueFileName=runTimePrefix+PhysicalName;
	
	XHQpwd = FetchXHQpwd();
	XHQHISTpwd = FetchXHQHISTpwd();
	XHQTNS = FetchTNSHost();
	MkXHQImpFile(XHQpwd);
	MkXHQImpFileHist(XHQpwd);
	MkXHQRescueImpFile(XHQpwd,RescueFileName);
	MkXHQHRescueImpFile(XHQHISTpwd,RescueFileNameHist);
	
	MkXHQRescFile(XHQpwd,RescueFileName);
	MkXHQRescFileHist(XHQpwd,RescueFileNameHist);
	MkXHQddl(XHQpwd);
	MkXHQddlHist(XHQpwd);
	MkXHQdml(XHQpwd);
	MkDataPumpDir(XHQpwd);

	WshSysEnv("oracle_sid")="XHQ";	
	if (!debug) WScript.echo (' Migration Utility '+cVersion+'\n' +
				 '  2012, Siemens Energy & Automation, Inc. \n\n' +
				 ' RESCUE - Importing Database Data ... \n');

	var BtnCode = WshShell.Popup("Do you want to proceed with the Database Rescue?",500, "                      *** WARNING ***", 4 + 16);
	if (BtnCode == 7) {
		WScript.echo ("Aborted!  Database has NOT been changed.");
		oFso.DeleteFile(CurrentDirectory+ "\\*.sql",true);
		oFso.DeleteFile(CurrentDirectory+ "\\*.par",true);
		if (oFso.FileExists(CurrentDirectory+ "\\ddlxhq.out")) 	oFso.DeleteFile(CurrentDirectory+ "\\ddlxhq.out",true);			
		WScript.Quit(1);
	}

	WScript.echo ("Please standby ... rescuing the database... ");
	var runDataPumpXHQ = WshShell.Run('sqlplus -S system/"'+ XHQpwd +'" @datapump.sql',1,true);
	var runDataPumpXHQ = WshShell.Run('sqlplus -S system/"'+ XHQpwd +'" @ddl.sql',1,true);
	if (XHQ_Major == 4)	{
		WshSysEnv("oracle_sid")="XHQHIST";
		var runDataPumpXHQHIST = WshShell.Run('sqlplus -S system/"'+XHQHISTpwd+'" @datapump.sql',1,true);
		var runDataPumpXHQHIST = WshShell.Run('sqlplus -S system/"'+XHQHISTpwd+'" @ddl_hist.sql',1,true);
	}		
	if (debug)
		WScript.echo ("Please standby... Importing ");

	WshSysEnv("oracle_sid")="XHQ";
	var ret = WshShell.Run('impdp parfile=XHQBKPIMPRescue.par',1,true);
	WScript.echo ('Database was restored from '+RescueFileName+'.dmp in '+CurrentDirectory);
	if (XHQ_Major == 4) { 
		WshSysEnv("oracle_sid")="XHQHIST";
		var ret = WshShell.Run('impdp parfile=XHQHBKPIMPRescue.par',1,true);
	}
	if (!debug) {
		oFso.DeleteFile(CurrentDirectory+ "\\*.sql",true);
		oFso.DeleteFile(CurrentDirectory+ "\\*.par",true);
	}
	WScript.echo ("End of the Migration Rescue Utility");
}

