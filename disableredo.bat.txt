@ECHO OFF
REM  $------------------------------------------------------------------$
REM      Copyright (c) 2004-2006 IndX Software Corp., a Siemens Company.
REM      All Rights Reserved.
REM
REM      This software is the confidential and proprietary information of IndX
REM      Software Corp., a Siemens Company ("Confidential Information").
REM
REM      You shall not disclose such Confidential Information and shall use it
REM      only in accordance with the terms of the license agreement you entered
REM      into with IndX.
REM
REM      CopyrightVersion 5.0
REM  $-----------------------------------------------------------------------$
REM  WRITTEN BY : Alex Ivascu
REM  PURPOSE    : Disable database to perform hot backups (noarchivelog mode)
REM  USAGE      : disableredo oracle_sid SysPassword
REM  $-----------------------------------------------------------------------$
REM
:begin

if "%1"=="" goto parmMissing
if "%2"=="" goto parmMissing

cls
echo Your parameters are : 
echo Oracle sid : %1
echo Sys user password : %2
echo ---------------------------------------------------------------------
echo Please make sure that there are no other users connected to this database !
echo Otherwise the script will wait indefinitely, until they log off from the database !
echo The script will put the database in noarchivelog mode ! 
echo That mean you^'ll not be able to make consistent, hot backups anymore.
set /p opt="Do you want to reconsider your option (Y/N) : " =

if /i %opt%==Y (goto retry)
set oracle_sid=%1

rem get database status 
rem -----------------------------------
(echo set echo off; & echo set pagesize 0 & echo set linesize 10 & echo set heading off; & echo set feedback off; & echo set recsep off; & echo spool status1.txt;)>get_status.sql
(echo select decode^(log_mode,^'ARCHIVELOG^',0,1^) from v^$database; & echo spool off; & echo exit;)>>get_status.sql
%xhq_db_home%\bin\sqlplus -s -L "sys/"%2" as sysdba" @get_status.sql
set /p stat1=<status1.txt
del /F get_status.sql
del /F status1.txt
set /a stat1=%stat1%
cls
if %stat1% EQU 1 goto already
echo shutdown immediate;                                            >> "disable.sql"
echo startup mount;                                                 >> "disable.sql"
echo alter database noarchivelog;                                   >> "disable.sql"
echo shutdown immediate;                                            >> "disable.sql" 
echo startup;                                                       >> "disable.sql"
echo exit;                                                          >> "disable.sql"
cls
ECHO $##############################################$
ECHO $----------------------------------------------$
ECHO $                                              $
ECHO $     Disable database for hot backups.        $
ECHO $             Please standby...                $
ECHO $                                              $
ECHO $    NOTE: This process can take as            $
ECHO $        long as 10-minutes.                   $
ECHO $                                              $
ECHO $----------------------------------------------$
ECHO $##############################################$
REM echo Disabling database for hot backups... a log file is written to disableredo.log
%xhq_db_home%\bin\sqlplus -s -L "sys/"%2" as sysdba" @disable.sql > disableredo.log 2>&1
del /F disable.sql
cls
findstr /I /C:"ERROR" disableredo.log
if errorlevel 0 if not errorlevel 1 goto logerrors
goto finished

:parmMissing
echo ------------------------------------------------------------------------------
ECHO ERROR: Please provide all the required parameters in order to run the script !
ECHO USAGE: disableredo "oracle sid" "sys password"
goto end

:finished
cls
echo SUCCESS:  Database is in noarchivelog mode. 
echo You^'ll not be able schedule hot backup job ...
goto end

:retry
echo -------------------------------------------------
echo WARNING: The script was not runned at this time !
echo Please review your parameters and run again.
goto end

:already
echo -------------------------------------------------------
echo Note: The database it's already in noarchivelog mode !
echo No modifications were performed.
goto end

:logerrors
echo ------------------------------------------
echo ERRORS found in disableredo.log
echo Please review or email support

:end
