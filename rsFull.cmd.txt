@echo off
REM  $-------------------------------------------------------------------------------------$
REM  Purpose: 	Full database recovery
REM  Usage: 	rsFull.cmd  oracle_sid SysPassword backup_directory_path working_directory
REM  $-------------------------------------------------------------------------------------$ 

if "%1"=="" goto parmMissing
if "%2"=="" goto parmMissing
if "%3"=="" goto parmMissing
if "%4"=="" goto parmMissing
if not exist %4 md %4
set oracle_sid=%1
for /F "delims=!" %%p in ('echo %3') do set back_dir=%%~p
for /F "delims=!" %%p in ('echo %4') do set working_dir=%%~p
set log_file=%working_dir%\restore_%oracle_sid%_script.log
set log_file1=%working_dir%\open_%oracle_sid%_script.log
rem find the most recent control file backup in the backup directory
set bkpctl=
for /f %%i in ('dir/B/O:D %back_dir%\ctrl_%oracle_sid%_*') do set bkpctl=%%i
if "%bkpctl%"=="" goto error1
rem find the most recent pfile and spfile backup in the backup directory
for /f %%i in ('dir/B/O:D %back_dir%\init%oracle_sid%.ora') do set pfile=%%i
if "%pfile%"=="" goto error2
for /f %%i in ('dir/B/O:D %back_dir%\spfile_%oracle_sid%_*') do set spfile=%%i
if "%spfile%"=="" goto error3
rem Parse DBID out of control file backup name
set dbid=
for /f "tokens=2 delims=-" %%i in ('echo %bkpctl%') do set dbid=%%i
rem Close database in case it's open !
(echo conn sys/"%2" as sysdba; & echo alter system archive log current; & echo alter system switch logfile; & echo alter system archive log current; & echo alter system checkpoint global; & echo shutdown abort; & echo exit;)> %working_dir%\rsFull.sql
%xhq_db_home%\bin\sqlplus /nolog @%working_dir%\rsFull.sql > %working_dir%\%oracle_sid%_shutdown.log 2>&1
rem Restore and recover the database script
(echo SET DBID = %dbid%; & echo run & echo { & echo startup nomount pfile="%back_dir%\%pfile%";)> %working_dir%\rsFull.rm 
(echo restore controlfile from '%back_dir%\%bkpctl%'; & echo restore spfile from ^'%back_dir%\%spfile%^'; & echo shutdown immediate;)>> %working_dir%\rsFull.rm
(echo startup mount; & echo restore database; & echo recover database; & echo } )>> %working_dir%\rsFull.rm
rem  Opening database script
(echo conn sys/"%2" as sysdba & echo alter database open resetlogs; & echo ^@^?\rdbms\admin\utlrp.sql & echo exit;) > %working_dir%\rsOpen.sql
cls
echo ------------------------------------------------------------
echo Restoring the %oracle_sid% database !                 
echo Please wait ...    
echo (depending on database size, this process can take a while)                
echo ------------------------------------------------------------
%xhq_db_home%\bin\rman target sys/"%2" nocatalog cmdfile %working_dir%\rsFull.rm > "%log_file%" 2>&1
%xhq_db_home%\bin\sqlplus /nolog @%working_dir%\rsOpen.sql > %log_file1% 2>&1
del %working_dir%\rsFull.sql
del %working_dir%\rsFull.rm
del %working_dir%\rsOpen.sql
cls
rem RMAN-06054 needs to be avoided since it's not actually an error ...
findstr /I /C:"RMAN-06054" "%log_file%"
if errorlevel 0 if not errorlevel 1 goto next
:next
findstr /I /C:"ERROR" "%log_file%"
if errorlevel 0 if not errorlevel 1 goto logerrors1
findstr /I /C:"ORA-" "%log_file1%"
if errorlevel 0 if not errorlevel 1 goto logerrors2

goto finished

:parmMissing
echo --------------------------------------------------------------------------------------
ECHO ERROR: Please provide all the required parameters in order to run the script !
ECHO USAGE: rsFull "oracle sid" "sys password" "backup directory path" "working directory"
echo --------------------------------------------------------------------------------------
goto end

:error1
cls
echo ------------------------------------------------------------------------------------------------------------------------
echo ERROR: No control files backup were found in directory %back_dir%. Please make sure that you entered the correct path ! 
echo ------------------------------------------------------------------------------------------------------------------------
goto end

:error2
cls
echo ------------------------------------------------------------------------------------------------------------------
echo ERROR: No pfile backup were found in directory %back_dir%. Please make sure that you entered the correct path ! 
echo ------------------------------------------------------------------------------------------------------------------
goto end

:error3
cls
echo ------------------------------------------------------------------------------------------------------------------
echo ERROR: No Spfile backup were found in directory %back_dir%. Please make sure that you entered the correct path ! 
echo ------------------------------------------------------------------------------------------------------------------
goto end

:logerrors1
echo -------------------------------------------------------
echo ERRORS found in %log_file% when restoring the database 
echo Please review or email support
echo -------------------------------------------------------
goto end

:logerrors2
echo -------------------------------------------------------
echo ERRORS found in %log_file% when opening the database 
echo Please review or email support
echo -------------------------------------------------------
goto end

:finished
cls
echo ---------------------------------------------
echo Database %oracle_sid% restored successfully.
echo ---------------------------------------------

:end
