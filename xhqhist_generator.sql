set serveroutput on;

create or replace
PROCEDURE xhqhist_generator(month_no  IN INTEGER DEFAULT 3, stream_id IN NUMBER)
AS
  v_cur_month   INTEGER(2);
  v_cur_year    INTEGER(4);
  v_first_month INTEGER(2);
  v_first_year  INTEGER(4);
  v_table_name  VARCHAR(15);
  v_table_count INTEGER(1);
  s_date date;
  e_date date;
PROCEDURE generate(tab_name IN VARCHAR2,  v_month IN INTEGER, v_year IN INTEGER,  v_stream_id IN NUMBER)
IS
inter varchar(2);
BEGIN
if v_month <10 then 
   inter := '0'||v_month;
else   
   inter := v_month;
end if;   
dbms_random.seed(10);
-- Only good quality, 262336
if ( to_date(v_year||inter||'01','yyyymmdd') = TRUNC(sysdate,'MM')) THEN
  execute immediate 'insert into xhqhist.XHQHT_'||v_month||'_'||v_year||' columns(STREAM_ID,TIME,QUALITY,V_REAL) select '||v_stream_id||', days.dd + sec.ss , 262336 , dbms_random.value(low => 0, high => 1000) from (select to_date('''||v_year||inter||'01'',''yyyymmdd'') + level-1 dd from dual connect by level<=extract(day from sysdate)) days,(select (level-1)/(24*60*60) ss from dual connect by level<=24*60*60) sec';
else
  execute immediate 'insert into xhqhist.XHQHT_'||v_month||'_'||v_year||' columns(STREAM_ID,TIME,QUALITY,V_REAL) select '||v_stream_id||', days.dd + sec.ss , 262336 , dbms_random.value(low => 0, high => 1000) from (select to_date('''||v_year||inter||'01'',''yyyymmdd'') + level-1 dd from dual connect by level<=extract(day from last_day(to_date('''||v_year||inter||'01'',''yyyymmdd'')))) days,(select (level-1)/(24*60*60) ss from dual connect by level<=24*60*60) sec';
end if;
END;
BEGIN
-- initial checks
if month_no < 1 OR month_no >12 THEN
 dbms_output.put_line('Incorrect input parameter ! No of months must be between 1 and 12 !');
 return;
end if;
-- check/adjust timeframe from XHQ_HIST_ADM_CONFIG table
select count(*) into v_table_count from xhqhist.XHQ_HIST_ADM_CONFIG; 
IF (v_table_count = 0) THEN 
  execute immediate 'insert into xhqhist.XHQ_HIST_ADM_CONFIG values(1,1, trunc(add_months(sysdate,-'||month_no||'),''MM'') ,sysdate)';
else
 begin 
  select start_date into s_date from xhqhist.XHQ_HIST_ADM_CONFIG;
  select end_date into e_date from xhqhist.XHQ_HIST_ADM_CONFIG;
  if trunc(add_months(sysdate,-month_no),'MM') < s_date then
     update xhqhist.XHQ_HIST_ADM_CONFIG set start_date = trunc(add_months(sysdate,-month_no),'MM');
  else
    if last_day(sysdate) > e_date then
       update xhqhist.XHQ_HIST_ADM_CONFIG set end_date = last_day(sysdate);
    end if;
  end if;
 end; 
end if;
commit;
--
v_cur_month   := extract(MONTH FROM sysdate);
v_cur_year    := extract(YEAR FROM sysdate);
v_first_month := extract(MONTH FROM add_months(sysdate, -month_no));
v_first_year  := extract(YEAR FROM add_months(sysdate,  -month_no));
-- check tables existence
WHILE ((v_cur_year > v_first_year) OR ((v_first_year = v_cur_year) AND (v_first_month <= v_cur_month)))
  LOOP
    v_table_name := 'XHQHT_'||v_first_month||'_' ||v_first_year;
    SELECT COUNT(*) INTO v_table_count FROM dba_tables where upper(table_name)=upper(v_table_name) and owner='XHQHIST';
    IF (v_table_count = 0) THEN
      execute immediate 'CREATE table xhqhist.'||v_table_name||' (STREAM_ID NUMBER(10) NOT NULL, TIME TIMESTAMP NOT NULL, QUALITY NUMBER(10) NOT NULL, V_INT INTEGER, V_REAL BINARY_FLOAT, V_STRING NVARCHAR2(2000), V_BOOLEAN CHAR(1), V_DECIMAL VARCHAR2(256), V_DOUBLE BINARY_DOUBLE, V_DATETIME TIMESTAMP, V_TIMEINTERVAL FLOAT, V_LONG NUMBER(20)) STORAGE(INITIAL 32M NEXT 32M) tablespace XHQ_HIST_DATA';
      execute immediate 'CREATE index xhqhist.IDX_'||v_table_name||'_STREAMTIME on xhqhist.'||v_table_name ||' (STREAM_ID, TIME DESC) STORAGE(INITIAL 16M NEXT 16M) tablespace XHQ_HIST_INDEX';
      execute immediate 'CREATE index xhqhist.IDX_'||v_table_name||'_TIME on xhqhist.'||v_table_name ||' (TIME) STORAGE(INITIAL 16M NEXT 16M) tablespace XHQ_HIST_INDEX';
      execute immediate 'ALTER table xhqhist.'||v_table_name||' add (CONSTRAINT FK_'||v_table_name||'_stream FOREIGN KEY (STREAM_ID) references xhqhist.XHQ_HIST_STREAM_CONFIG (STREAM_ID) on delete cascade)';
    END IF;
    -- generate the data for XHQHT_MM_YYYY table
    generate(v_table_name, v_first_month, v_first_year, stream_id);
    v_first_month := v_first_month + 1;
    IF (v_first_month = 13) THEN
      v_first_month  := 1;
      v_first_year   := v_first_year + 1;
    END IF;
  END LOOP;
END;
/