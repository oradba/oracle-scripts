@ECHO OFF
set pass=%1
set p_path=%6
setlocal enableDelayedExpansion
REM  $---------------------------------------------------------------------------------------------------------$
REM  WRITTEN BY : ITVIZION
REM  PURPOSE    : Generate history data for XHQHIST instance  
REM  USAGE      : xhqhist_generator.cmd  sys_password no_of_month working_directory OID MID Full_Path Data_Type
REM  $---------------------------------------------------------------------------------------------------------$
REM

:begin
if "%1"=="" goto parmMissing
if "%2"=="" goto parmMissing
if "%3"=="" goto parmMissing
if "%4"=="" goto parmMissing
if "%5"=="" goto parmMissing
if "%6"=="" goto parmMissing
if "%7"=="" goto parmMissing
set p_months=%2
if !p_months! LSS 1 goto logerrors0
if !p_months! GTR 12 goto logerrors0
if not exist %3 md %3
for /F "delims=!" %%p in ('echo %3') do set working_dir=%%~p
set log_file=%working_dir%\xgenerator.log
set p_oid=%4
set p_mid=%5
set p_typ=%7
call :ToUpCase "%p_typ%" p_typ 
set /a p_dat_typ=0
if "%p_typ%"=="INTEGER" set p_dat_typ=15
if "%p_typ%"=="LONG" set p_dat_typ=16
if "%p_typ%"=="REAL" set p_dat_typ=17
if "%p_typ%"=="DOUBLE" set p_dat_typ=18
if !p_dat_typ! EQU 0 goto :logerrors3

REM Getting SolutionId from XHQ instance
(echo WHENEVER SQLERROR EXIT SQL.SQLCODE; & echo set echo off; & echo set pagesize 0 & echo set linesize 50 & echo set heading off; & echo set feedback off; & echo set recsep off; & echo select sysdate from dual;)>%working_dir%\get_solid.sql
(echo spool %working_dir%\solid.txt; & echo select solution_id from xhq.XHQ_SOLUTION; & echo spool off; & echo exit;)>>%working_dir%\get_solid.sql
sqlplus -s -L sys/!pass!@XHQ as sysdba @%working_dir%\get_solid.sql
del %working_dir%\get_solid.sql
if not exist %working_dir%\solid.txt (
(echo Warning !!! & echo There is a problem getting the SolutionId for the XHQ instance !)>%log_file%
goto dbError
)
set /p solid=<%working_dir%\solid.txt
set solid=!solid: =!
del %working_dir%\solid.txt

REM Getting StreamId from XHQHIST instance 
(echo WHENEVER SQLERROR EXIT SQL.SQLCODE; & echo set echo off; & echo set pagesize 0 & echo set linesize 50 & echo set heading off; & echo set feedback off; & echo set recsep off; & echo select sysdate from dual;)>%working_dir%\get_stid.sql
(echo spool %working_dir%\stid.txt; & echo select xhqhist.XHQ_HIST_STREAM_SEQ.nextval from dual; & echo spool off; & echo exit;)>>%working_dir%\get_stid.sql
sqlplus -s -L sys/!pass!@XHQHIST as sysdba @%working_dir%\get_stid.sql
del %working_dir%\get_stid.sql
if not exist %working_dir%\stid.txt (
(echo Warning !!! & echo There is a problem getting the SolutionId for the XHQ instance !)>%log_file%
goto dbError
)
set /p stid=<%working_dir%\stid.txt
set stid=!stid: =!
del %working_dir%\stid.txt

REM Inserting the master row in XHQ_HIST_STREAM_CONFIG table
(echo Inserting the master row in XHQ_HIST_STREAM_CONFIG table !)>%log_file%
(echo spool %working_dir%\insert_row.log; & echo insert into xhqhist.XHQ_HIST_STREAM_CONFIG values^(^'%solid%^',%stid%, %p_oid%, %p_mid%,^'%p_path%^',%p_dat_typ%,0,1^)^;)>%working_dir%\ins_row.sql
(echo commit; & echo spool off; & echo exit;)>>%working_dir%\ins_row.sql
sqlplus -s -L sys/!pass!@XHQHIST as sysdba @%working_dir%\ins_row.sql
del %working_dir%\ins_row.sql
findstr /I /C:"ERROR" "%working_dir%\insert_row.log" > nul
if errorlevel 0 if not errorlevel 1 goto logerrors1

REM Inserting/generating rows in XHQHT_MM_YYYY tables
cls
echo Generating the main data. Please wait ...
(echo Inserting^/generating rows in XHQHT_MM_YYYY tables ^!)>>%log_file%
(echo spool %working_dir%\insert_rows.log; & echo exec xhqhist_generator^(%p_months%,%stid%^);)>%working_dir%\ins_rows.sql
(echo spool off; & echo exit;)>>%working_dir%\ins_rows.sql
sqlplus -s -L sys/!pass!@XHQHIST as sysdba @%working_dir%\ins_rows.sql
del %working_dir%\ins_rows.sql
findstr /I /C:"ERROR" "%working_dir%\insert_rows.log" > nul
if errorlevel 0 if not errorlevel 1 goto logerrors2

findstr /I /C:"ERROR" "%log_file%" > nul
if errorlevel 0 if not errorlevel 1 goto logerrors
goto finished

:ToUpCase
set var_=%~1
for %%c in (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z � � �) do (
  set var_=!var_:%%c=%%c!
)
set %2=%var_%& 
goto end

:logerrors
cls
echo ERRORS found in "%log_file%"
echo Please review and/or email support.
goto end

:logerrors0
cls
echo There is an issue with the parameters. 
echo The number of months must be between 1 and 12 !
echo Please rerun the script with the month parameter within this range !
goto end

:logerrors1
cls
echo There is an issue with the parameters. 
echo The provided input parameters cannot be inserted in XHQ_HIST_STREAM_CONFIG table !
echo ERRORS found in %working_dir%\insert_row.log
echo Please review and/or email support.
goto end

:logerrors2
cls
echo There was an issue during the generation of data !
echo ERRORS found  in %working_dir%\insert_rows.log
echo Please review and/or email support.
goto end

:logerrors3
cls
echo There is an issue with the parameters. 
echo The Data_Type parameter can only be ^: integer, real, long or double !
echo Please rerun the script with the allowed value for this parameter !
goto end

:finished
cls
echo SUCCESS:  XHQHIST generator succeeded. 
goto end

:parmMissing
echo ----------------------------------------------------------------------------------------------------------
ECHO ERROR: Please provide all the required parameters in order to run the script !
ECHO USAGE: xhqhist_generator "sys password" "no of months" "working directory" "OID" "MID" "PATH" "DATA_TYPE"
goto end

:retry
echo -------------------------------------------------
echo WARNING: The script was not ran at this time ^!
echo Please review your parameters and run again.
goto end

:dbError
echo ----------------------------------------------------------------------------------
echo Warning : There was a problem getting the data for oracle instance ^!
echo Please check the log file log_file ^!
goto end

:end
endlocal
