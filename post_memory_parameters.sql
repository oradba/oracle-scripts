set serveroutput on;
set linesize 1000;
DECLARE
  v_mem varchar2(50);
  sem varchar2(1);
BEGIN
sem:='0';
dbms_output.put_line(lpad('>',100,'-'));
select decode(value,null,1,0) into v_mem from v$parameter where name='spfile';
if v_mem >=1
then 
 dbms_output.put_line('The script cannot run because Oracle instance is started using a pfile and not an spfile !');
 dbms_output.put_line('Please start the oracle instance using an spfile !');
 goto  noneed;
end if;
select value into v_mem from v$parameter where name='memory_max_target';
if v_mem > 0
then
 dbms_output.put_line ('Reseting MEMORY_MAX_TARGET');
 execute immediate 'alter system reset memory_max_target scope=spfile';
 sem:='1';
end if;
select value into v_mem from v$parameter where name='memory_target';
if v_mem > 0
then
 dbms_output.put_line ('Reseting MEMORY_TARGET');
 execute immediate 'alter system reset memory_target scope=spfile';
 sem:='1';
 end if;
select value into v_mem from v$parameter where name='sga_max_size';
IF v_mem < '2147483648'
 THEN
   dbms_output.put_line ('Increasing SGA_MAX_SIZE to minimum required');
   execute immediate 'alter system set sga_max_size=2G scope=spfile';
   execute immediate 'alter system set sga_target=2G scope=spfile';
   sem:='1';
 ELSE
  dbms_output.put_line ('SGA_MAX_SIZE is at least 2GB. No change needed.');
END IF;
select value into v_mem from v$parameter where name='pga_aggregate_target';
if v_mem < '2147483648'
then
 dbms_output.put_line ('Increasing PGA_AGGREGATE_TARGET to minimum required');
 execute immediate 'alter system set pga_aggregate_target=2G scope=spfile';
 sem:='1';
else
 dbms_output.put_line ('PGA_AGGREGATE_TARGET is at least 2GB. No change needed.');
end if;
if sem > '0'
then 
 dbms_output.put_line('Now you need to restart the application (oracle included) !');
end if;
<<noneed>>
null;
END;
/
exit;