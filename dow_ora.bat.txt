@ECHO OFF
REM  $-----------------------------------------------------------------------------------------------$
REM  WRITTEN BY : Alex Ivascu
REM  PURPOSE    : Create a new user (identified externally) plus some additional profile and roles
REM               It also set utl_file_dir so database restart it's involved   
REM  USAGE      : dow_ora oracle_sid sysPassword "destination directory" "new_user" "sequence value"
REM  $-----------------------------------------------------------------------------------------------$
REM
:begin

if "%1"=="" goto parmMissing
if "%2"=="" goto parmMissing
if "%3"=="" goto parmMissing
if "%4"=="" goto parmMissing
if "%5"=="" goto parmMissing
set usern=%4
cls
echo Your parameters are : 
echo Oracle sid : %1
echo Sys user password : %2
echo Utl_file_dir location : %3 
echo Value for sequence SEQ_REASN_SUB_LOC_ID : %usern%
echo ---------------------------------------------------------------------
echo Please make sure that there are no other users connected to this database !
echo Otherwise the script will wait indefinitely, until they log off from the database !
set /p opt="Do you want to reconsider your option (Y/N) : " =
if /i %opt%==Y (goto retry)
if not exist %3 md %3
set oracle_sid=%1
rem get database status 
rem -----------------------------------
(echo set echo off; & echo set pagesize 0 & echo set linesize 10 & echo set heading off; & echo set feedback off; & echo set recsep off; & echo spool status1.txt;)>get_status.sql
(echo select nvl^(length^(value^),0^) from v^$parameter where name=^'spfile^'; & echo spool status2.txt; & echo select count^(^*^) from dba_users where username=^'%usern%^'; & echo spool off; & echo exit;)>>get_status.sql
sqlplus -s -L "sys/"%2" as sysdba" @get_status.sql
set /p stat1=<status1.txt
set /p stat2=<status2.txt 
del /F get_status.sql
del /F status1.txt
del /F status2.txt
if %stat1% EQU 0 goto notnow
if %stat2% EQU 1 goto already
rem Main script here !
rem --------------------
(echo grant select any dictionary to xhq; & echo alter system set utl_file_dir=^'%3%^' scope=spfile; & echo alter system set os_authent_prefix=^'^' scope=spfile; & echo shutdown immediate; & echo startup;)>main_script.sql
(echo alter trigger xhq.XHQ_OA_RDSL_LOG_TRIG compile; & echo alter trigger xhq.XHQ_OA_RDSL_LOG_TRIG ENABLE; & echo drop profile "DOW_FUNCTIONAL"; & echo CREATE PROFILE "DOW_FUNCTIONAL" & echo LIMIT & echo COMPOSITE_LIMIT DEFAULT & echo SESSIONS_PER_USER DEFAULT)>>main_script.sql
(echo CPU_PER_SESSION DEFAULT & echo CPU_PER_CALL DEFAULT & echo LOGICAL_READS_PER_SESSION DEFAULT & echo LOGICAL_READS_PER_CALL DEFAULT & echo IDLE_TIME DEFAULT & echo CONNECT_TIME DEFAULT & echo PRIVATE_SGA DEFAULT)>>main_script.sql
(echo FAILED_LOGIN_ATTEMPTS 3 & echo PASSWORD_LIFE_TIME UNLIMITED & echo PASSWORD_REUSE_TIME UNLIMITED & echo PASSWORD_REUSE_MAX DEFAULT & echo PASSWORD_VERIFY_FUNCTION DEFAULT & echo PASSWORD_LOCK_TIME UNLIMITED)>>main_script.sql
(echo PASSWORD_GRACE_TIME UNLIMITED; & echo CREATE USER "%usern%" IDENTIFIED EXTERNALLY DEFAULT TABLESPACE XHQ_DATA_SMALL TEMPORARY TABLESPACE TEMP PROFILE DOW_FUNCTIONAL;)>>main_script.sql
(echo drop role XHQ_RDS_UPDT; & echo drop role XHQ_CACHE_SEL; & echo CREATE ROLE XHQ_RDS_UPDT; & echo CREATE ROLE XHQ_CACHE_SEL;)>>main_script.sql
(echo grant XHQ_RDS_UPDT to "%usern%"; & echo grant XHQ_CACHE_SEL to "%usern%";)>>main_script.sql
(echo drop sequence xhq.SEQ_REASN_SUB_LOC_ID; & echo create sequence xhq.SEQ_REASN_SUB_LOC_ID start with %5% increment by 1 nocache nocycle;)>>main_script.sql
rem Second script here
-------------------------
(echo set echo off; & echo set pagesize 0 & echo set linesize 200 & echo set heading off; & echo set feedback off; & echo set recsep off; & echo spool main_script.sql append;)>dow_s1.sql
echo select ^'create or replace public synonym ^'^|^|object_name^|^|^' for XHQ.^'^|^|object_name^|^|^';^' from all_objects where owner=^'XHQ^' and object_name not like ^'XHQ_%%^' and object_type =^'TABLE^';>>dow_s1.sql
echo select ^'create or replace public synonym ^'^|^|object_name^|^|^' for XHQ.^'^|^|object_name^|^|^';^' from all_objects where owner=^'XHQ^' and object_type =^'SEQUENCE^';>>dow_s1.sql
echo select ^'grant update, select, insert, delete on XHQ.^'^|^|object_name^|^|^' to XHQ_RDS_UPDT;^' from all_objects where owner=^'XHQ^' and object_name not like ^'XHQ_%%^' and object_type =^'TABLE^';>>dow_s1.sql
echo select ^'grant select on XHQ.^'^|^|object_name^|^|^' to XHQ_CACHE_SEL;^' from all_objects where owner=^'XHQ^' and object_name not like ^'XHQ_%%^' and object_type =^'TABLE^';>>dow_s1.sql
(echo select ^'grant select on XHQ.^'^|^|object_name^|^|^' to XHQ_CACHE_SEL;^' from all_objects where owner=^'XHQ^' and object_type =^'SEQUENCE^'; & echo select ^'exit;^' from dual; & echo spool off; & echo exit;)>>dow_s1.sql
sqlplus -s -L "sys/"%2" as sysdba" @dow_s1.sql
del /F dow_s1.sql
cls
ECHO $##############################################$
ECHO $----------------------------------------------$
ECHO $                                              $
ECHO $       Executing the main oracle script.      $
ECHO $              Please standby...               $
ECHO $                                              $
ECHO $       NOTE: This process can take as         $
ECHO $             long as 10-minutes.              $
ECHO $                                              $
ECHO $----------------------------------------------$
ECHO $##############################################$

REM Executing the main oracle script ... a log file is written to addredolog.log"
sqlplus -s -L "sys/"%2" as sysdba" @main_script.sql > dow_ora.log 2>&1
rem del /F main_script.sql
findstr /I /C:"ERROR" dow_ora.log
if errorlevel 0 if not errorlevel 1 goto logerrors
goto finished

:parmMissing
echo ------------------------------------------------------------------------------
ECHO ERROR: Please provide all the required parameters in order to run the script !
ECHO USAGE: dow_ora oracle_sid sys_Password destination_directory_for_Utl_file_dir new_user_name sequence_value
goto end

:finished
cls
echo SUCCESS:  User created and parameter set. 
echo You can now use "%4%" user.
goto end

:retry
echo -------------------------------------------------
echo WARNING: The script was not runned at this time !
echo Please review your parameters and run again.
goto end

:already
echo -------------------------------------------------------
echo Note: The user "%4%" it's already present in the database !
echo No modifications were performed.
goto end

:notnow
echo ----------------------------------------------------------------------------------------------
echo WARNING: The script was not runned at this time becouse the database is started with a pfile !
echo Please use a spfile to start the database then run again.
goto end

:logerrors
echo ------------------------------------------
echo ERRORS found in dow_ora.log
echo Please review or email support

:end




