@ECHO OFF
setlocal enableDelayedExpansion
REM  $------------------------------------------------------------------------------------------------------------------$
REM  PURPOSE    : Truncate internal statistics tables and set statistics retention to 1 day for XHQCACHE oracle instance 
REM  USAGE      : fix_stats_retention.cmd sys_password working_directory
REM  $------------------------------------------------------------------------------------------------------------------$

if "%1"=="" goto parmMissingOrWrong
if "%2"=="" goto parmMissingOrWrong
if not exist %2 md %2
set oracle_sid=XHQCACHE
for /F "delims=!" %%p in ('echo %2') do set working_dir=%%~p
set log_file=%working_dir%\set_stats.log
(echo truncate table SYS^.WRI^$^_OPTSTAT^_HISTHEAD^_HISTORY; & echo truncate table SYS^.WRI^$^_OPTSTAT^_TAB^_HISTORY;)>%working_dir%\set_stats.sql
(echo truncate table SYS^.WRI^$^_OPTSTAT^_IND_HISTORY; & echo truncate table sys^.wri^$^_optstat^_histgrm_history; & echo exec dbms_stats^.alter^_stats^_history^_retention^(retention^=^>1^); & echo exit;)>>%working_dir%\set_stats.sql
sqlplus -s -L sys/"%1" as sysdba @%working_dir%\set_stats.sql > %log_file% 2>&1
del %working_dir%\set_stats.sql

findstr /I /C:"ERROR" "%log_file%"
if errorlevel 0 if not errorlevel 1 goto logerrors
goto finished

:logerrors
cls
echo ERRORS found in "%log_file%"
echo Please review and/or email support.
goto end

:finished
cls
echo ------------------------------------------------------------------------------
echo SUCCESS:  The stats were truncated and the retention was set to 1 day ! 
goto end

:parmMissingOrWrong
echo --------------------------------------------------------------------------------------------------
ECHO ERROR: Please provide the required parameters in order to run the script !
ECHO USAGE: fix_stats_retention.cmd sys_password^(for Xhqcahe instance^) working_directory^(for log^)
echo --------------------------------------------------------------------------------------------------
goto end

:end
endlocal