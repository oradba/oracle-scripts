@echo off
rem Setting variables
set ORACLE_HOME=C:\Oracle\product\11.2.0\dbhome_2
set ORACLE_SID=%1
set my_path=C:\oracle\backup_script\
rman target / cmdfile '%my_path%arch_backup.rm' log '%my_path%Archive_"%1"_backup.log' using %1 %2
find "ORA-" %my_path%Archive_%1_backup.log>NUL && (blat -to dba@itvizion.com -server smtpvip.3ecorp.com -f backup_script@alerts.com -subject "Error alert" -body "There is an ORA error in backup log for "%1" instance on oracle08 ! | |Please check the attached file !" -attach %my_path%Archive_%1_backup.log)
exit