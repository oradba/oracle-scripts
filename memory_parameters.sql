set serveroutput on;
set linesize 1000;
DECLARE
  v_mem varchar2(50);
BEGIN
dbms_output.put_line(lpad('>',100,'-'));
select decode(value,null,1,0) into v_mem from v$parameter where name='spfile';
if v_mem >=1
then 
 dbms_output.put_line('The script cannot run because Oracle instance is started using a pfile and not an spfile !');
 dbms_output.put_line('Please start the oracle instance using an spfile !');
 goto  noneed;
end if;
select value into v_mem from v$parameter where name='memory_max_target';
if v_mem >=2147483648
then
  dbms_output.put_line('No changes needed here ! There is no need to restart the oracle instance !');
  goto noneed;
end if;
select value into v_mem from v$parameter where name='memory_target';
if v_mem >=2147483648
then
 dbms_output.put_line('Seting only MEMORY_MAX_TARGET !');
 execute immediate 'alter system set memory_max_target='||to_char(v_mem)||' scope=spfile';
 goto rneed;
else 
-- no memory parameter is set
  dbms_output.put_line('Seting both memory parameters : MEMORY_MAX_TARGET and MEMORY_TARGET !'); 
  select value into v_mem from v$parameter where name='sga_max_size';
  if v_mem = '0'
  then 
   select sum(value) into v_mem from v$parameter where name in ('sga_target','pga_aggregate_target'); 
  else 
   select sum(value) into v_mem from v$parameter where name in ('sga_max_size','pga_aggregate_target'); 
  end if;  
  if v_mem >='2147483648'  
-- set the memory parameters large enough to accomodate the existing SGA and PGA
  then 
    execute immediate 'alter system set memory_max_target='||to_char(v_mem+33554432)||' scope=spfile';
    execute immediate 'alter system set memory_target='||to_char(v_mem+33554432)||' scope=spfile';
  else  
    execute immediate 'alter system set memory_max_target=2147483648 scope=spfile';
    execute immediate 'alter system set memory_target=2147483648 scope=spfile';
  end if;
end if;
<<rneed>>
dbms_output.put_line('Now you need to restart the application (oracle included) !');
<<noneed>>
null;
END;
/
exit;