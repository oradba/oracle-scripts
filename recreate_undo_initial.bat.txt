@echo off
REM  $------------------------------------------------------------------$
REM      Copyright (c) 2013 Siemens Energy, Inc.
REM  $------------------------------------------------------------------$
REM  WRITTEN BY : ItVizion
REM  PURPOSE    : Drop an recreate undo tablespace for an OMF instance
REM  USAGE      : recreate_undo.bat
REM  $------------------------------------------------------------------$
set log_file=recreate_undo.log

if not "%1"=="" goto howto
cls
setlocal enableextensions enabledelayedexpansion
set orasid=
set /p orasid=What is your oracle instance name (default value XHQ ) ? : 
if "%orasid%"=="" set orasid=XHQ
set /p Pwd=What is your SYSTEM user password ? : 
set ORACLE_SID=!orasid!
echo ######################################
echo #   Check if it's an OMF instance    #
echo ######################################
(echo . & echo . & echo . & echo Started %date% %time% & echo Check if we have an OMF instance ...)>%log_file%
(echo WHENEVER SQLERROR EXIT SQL.SQLCODE; & echo set echo off; & echo set pagesize 0 & echo set linesize 10 & echo set heading off; & echo set feedback off; & echo set recsep off; & echo select sysdate from dual;)>inter.sql
(echo spool omf.txt; & echo select sum^(decode^(value,null,0,'',0,' ',0,1^)^) from v^$parameter where name='db_create_file_dest'; & echo spool off; & echo exit;)>>inter.sql
sqlplus -s -l system/"!Pwd!" @inter.sql
if not exist omf.txt (
echo.
echo ^-^-^>^> Warning ^! 
echo There is a problem with your data ^! 
echo Make sure you have the right oracle sid and system user password then try again ^!
(echo Warning ^! & echo There was a problem with incorrect oracle sid or system user password ^!)>>%log_file%  
goto end
)
cls
set /p Is_Omf=<omf.txt
if %Is_Omf% == 0 (
echo.
echo This it's not an OMF instance ^! 
echo The script will stop here ^! 
echo The undo tablespace was not recreated ^!
echo Please contact support.
(echo This it's not an OMF instance ^! The script will stop here ^! & echo The undo tablespace was not recreated ^!)>>%log_file%
goto end
)
echo #####################################
echo #   create the new undo tablespace  #
echo #   and drop the current one        # 
echo #####################################
rem get the current undo tablespace name
(echo set echo off; & echo set pagesize 0 & echo set linesize 20 & echo set heading off; & echo set feedback off; & echo set recsep off;)>inter.sql
(echo spool omf.txt; & echo SELECT value FROM v^$parameter WHERE name='undo_tablespace'; & echo spool off; & echo exit;)>>inter.sql
sqlplus -s -l system/"!Pwd!" @inter.sql
set /p undoname=<omf.txt
set undoname=%undoname: =%
if %undoname%==UNDOTBS1 (
set newundoname=UNDOTBS2
) else (
set newundoname=UNDOTBS1)
Rem Using the sleep in order to allow current transactions to finish, before droppig the current undo tablespace
echo Working ...
echo Starting to drop and recreate a new undo tablespace>>%log_file% 
(echo set echo off; & echo set pagesize 0 & echo set heading off; & echo set feedback off; & echo set recsep off; & echo spool %log_file% append ;)>inter.sql
(echo create undo tablespace %newundoname% datafile size 512M AUTOEXTEND ON NEXT 128M MAXSIZE UNLIMITED EXTENT MANAGEMENT LOCAL; & echo alter system set undo_tablespace=%newundoname% ;)>>inter.sql
(echo alter tablespace %undoname% offline; & echo exec dbms_lock.sleep^(60^); & echo drop tablespace %undoname% including contents and datafiles; & echo spool off; & echo exit;)>>inter.sql
sqlplus -s -l system/"!Pwd!" @inter.sql
echo Undo tablespace recreated ^!
echo Undo tablespace recreated ^!>>%log_file% 
goto end
:howto
cls
echo ####################################    
echo #   No parameters expected...      # 
echo #   USAGE: recreate_undo.bat       #
echo ####################################                                                            
goto end

:end
rem del recreate_undo.log
del inter.sql
del omf.txt
echo End of script at %date% %time% !>>%log_file%
echo.
echo ###########################################
echo #  script recreate_undo ran successfully  #
echo ###########################################
